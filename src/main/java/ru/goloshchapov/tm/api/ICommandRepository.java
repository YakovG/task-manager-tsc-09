package ru.goloshchapov.tm.api;

import ru.goloshchapov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
